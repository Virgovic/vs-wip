#!/usr/bin/env bats

if [ "$(id -u)" != "0" ]
then
  echo "Script must be run as root" 1>&2
  exit 1
fi

DATA_DEVICE="testDevice"
HASH_DEVICE="testHash"
DEVICE_NAME="testVerity"
FEC_DEVICE="test_fec"
VERITYSETUP="src/veritysetup"
 
# Create device with 256 K.
@test "::: setup ::: create data device" {
 run dd if=/dev/zero of=$DATA_DEVICE bs=1K count=256
 [ "$status" -eq "0" ]

 run dd if=/dev/zero of=$FEC_DEVICE bs=1K count=256
 [ "$status" -eq "0" ]
}

# Check whether pregram veritysetup is present.
@test "Check veritysetup :: availibility of program" {

 command $VERITYSETUP --help 1>/dev/null

}

@test "Check veritysetup :: functionality :: wrong parameters" {
 
 run $VERITYSETUP format $DATA_DEVICE
 [ "$status" = "1" ]

}

@test "Check veritysetup :: functionality :: wrong device" {
 
 run $VERITYSETUP format "wrong_device" $HASH_DEVICE
 [ "$status" = "4" ]

}

@test "Check veritysetup :: functionality :: format device" {
 
 run $VERITYSETUP format $DATA_DEVICE $HASH_DEVICE
 [ "$status" = "0" ]

}

@test "Check veritysetup :: functionality :: data/hash device availability" { 
 [ -f "$DATA_DEVICE" ]
 [ -f "$HASH_DEVICE" ] 
}

@test "Check veritysetup :: functionality :: hash device availability" {
   
 run $VERITYSETUP dump $HASH_DEVICE
 [ "$status" = "0" ]

}

@test "Check veritysetup :: functionality :: root hash, create mapping, verify" {
 #$VERITYSETUP format $DATA_DEVICE $HASH_DEVICE
 ROOT_HASH=`$VERITYSETUP format $DATA_DEVICE $HASH_DEVICE | grep 'Root hash:'`
 ROOT_HASH=${ROOT_HASH:10}
 ROOT_HASH=`echo $ROOT_HASH | xargs`
 export ROOT_HASH=$ROOT_HASH
 [ "${#ROOT_HASH}" -eq "64" ]
 run $VERITYSETUP create $DEVICE_NAME $DATA_DEVICE $HASH_DEVICE $ROOT_HASH
 [ "$status" -eq "0" ]
 [ -e "/dev/mapper/$DEVICE_NAME" ] 
 run $VERITYSETUP verify $DATA_DEVICE $HASH_DEVICE $ROOT_HASH
 [ "$status" -eq "0" ]
}

@test "Check veritysetup :: functionality :: status" {
 run $VERITYSETUP status $DEVICE_NAME
 [ "$status" = 0 ]
 [[ "${lines[0]}" =~ "/dev/mapper/$DEVICE_NAME is active." ]]
}

#@test "Check veritysetup :: FEC :: functionality" {
 #8aee89c2988491c8d2b5623800504137dc803bfcada1a91b8a65bf0c3f23db60

#}

@test "::: cleanup ::: remove hash, data device, remove mapping" {
 
 [ -e "$HASH_DEVICE" ] && rm $HASH_DEVICE
 [ -e "$DATA_DEVICE" ] && rm $DATA_DEVICE 
 [ -e "$DATA_DEVICE" ] && rm $FEC_DEVICE
 [ -e "/dev/mapper/$DEVICE_NAME" ] && $VERITYSETUP remove $DEVICE_NAME     

}
